<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testRedirectFromDashboardIfNotLoggedIn()
    {
        $response = $this->get( route('dashboard.index'));
        $response->assertStatus(302);
    }

    public function testAccessToDashboardIfLoggedIn()
    {
        $user     = factory(\App\User::class)->create();
        $response = $this->actingAs($user, 'web')->get(route('dashboard.index'));
        $response->assertStatus(200);
        $response->assertSee('Dashboard');
        $user->delete();
    }

    public function testAnyoneCanAccessRoot()
    {
        $response = $this->get( '/');
        $response->assertStatus(200);
        $response->assertSee('Test Exercise');
    }
}
