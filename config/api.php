<?php
return [
    'base_uri' => env('EVENTS_BASE_URI', 'https://app.ticketmaster.com/discovery/v2'),
    'events' => [
        'endpoint' => env('EVENTS_ENDPOINT', '/events.json')
    ],
];
