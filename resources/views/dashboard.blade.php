@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard - Events</div>



                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                    @if(!isset($error))
                            <form action="{{route('dashboard.index')}}" method="get" class="p-2">
                                <div class="form-group">
                                    <label for="searchByKeyword">Search By Keyword</label>
                                    <input id="searchByKeyword" type="text" name="keyword" class="form-control"/>
                                </div>
                                <input type="submit" class="btn btn-outline-primary" value="Search By Keyword">

                            </form>
                            @php
                                /** @var \App\Models\Event\Event $event */
                                /** @var \App\Repositories\Common\Pagination $pagination */
                            @endphp
                            @if ($pagination->hasPreviousPage())
                                <a  class="btn btn-outline-info d-inline-blockl m-2" href="{{route('dashboard.index') .'?'. $query . '&page=' . $pagination->getPreviousPage() . '&size=' . $pagination->getPageSize()}}">Prev</a>
                            @endif
                            @if ($pagination->hasNextPage())
                                <a class="btn btn-outline-info d-inline-block m-2" href="{{route('dashboard.index') .'?'. $query . '&page=' . $pagination->getNextPage() . '&size=' . $pagination->getPageSize()}}">Next</a>
                            @endif
                        @endif

                    <table class="table table-striped">
                        <tr>
                            <th>Name</th>
                        </tr>
                        @if (isset($error))
                            <tr class="alert alert-danger">
                                <td>{{$error}}</td>
                            </tr>
                        @else
                        @foreach ($events as $event)
                            <tr>
                                <td>
                                    <a target="_blank" href="{{$event->getUrl()}}">{{$event->getName()}}</a><br />
                                    <small class="text-muted">Type: {{$event->getType()}}</small>
                                </td>
                            </tr>
                        @endforeach
                        @endif
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
