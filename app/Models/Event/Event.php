<?php
namespace App\Models\Event;

/**
 * Interface Event
 * @package App\Models\Event
 */
interface Event
{
    /**
     * @return string
     */
    public function getId(): string;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return string
     */
    public function getUrl(): string;
}
