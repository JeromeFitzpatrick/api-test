<?php
namespace App\Models\Event;

use App\Models\Common\BasicPopo;

/**
 * Class GenericModel
 * @package App\Models\Event\Api
 */
class EventModel extends BasicPopo implements Event
{
    /**
     * @var
     */
    protected $id;
    /**
     * @var
     */
    protected $name;
    /**
     * @var
     */
    protected $type;
    /**
     * @var
     */
    protected $url;
    /**
     * @var array
     */
    protected $attributes = [];
    /**
     * @var array
     */
    protected $config = [
        'id' => ['type'=>'string',],
        'name' => ['type'=>'string',],
        'type' => ['type'=>'string',],
        'url' => ['type'=>'string',],
    ];

    /**
     * EventModel constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
        $this->hydrate();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

}
