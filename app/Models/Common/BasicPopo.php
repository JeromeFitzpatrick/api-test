<?php


namespace App\Models\Common;


abstract class BasicPopo
{
    protected $objectType = 'basic popo object';
    protected $config;
    protected $attributes;

    protected function hydrate()
    {
        foreach ($this->config as $attributeName => $rules) {
            $this->validateAttribute($attributeName);
            $this->$attributeName = $this->attributes[$attributeName] ?? null;
        }
    }

    protected function validateAttribute(string $attributeName)
    {
        if (!$value = $this->attributes[$attributeName] ?? null) {
            $this->attributes[$attributeName] = $value;
        }

        if (!is_null($value) && gettype($value) != $this->config[$attributeName]['type']) {
            throw new \InvalidArgumentException("{$attributeName} is of the wrong type when trying to create  {$this->objectType}. Expected: " .
                $this->config[$attributeName]['type'] . ", but received: " . gettype($value));
        }
    }
}
