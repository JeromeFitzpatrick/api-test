<?php
namespace App\Repositories\Api;


use App\Models\Event\EventModel;
use App\Repositories\Common\PaginationPopo;
use App\Repositories\EventRepository;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\Collection;

/**
 * Class ApiEventRepository
 * @package App\Repositories\Api
 */
class ApiEventRepository extends ApiRepository implements EventRepository
{
    /**
     * ApiEventRepository constructor.
     * @param ClientInterface $apiClient
     */
    public function __construct(ClientInterface $apiClient)
    {
        parent::__construct($apiClient);
    }

    /**
     * @return Collection
     */
    public function get(): Collection
    {
        //TODO: handle error here an return throw specific custom error

        $body = parent::get();
        $dataCollection = collect($body['_embedded']['events'] ?? []);
        $pageCollection = collect([$body['page']]);

        $eventCollection = $dataCollection->mapInto(EventModel::class);
        $pagination = $pageCollection->mapInto(PaginationPopo::class);

        return collect(['data' => $eventCollection, 'pagination'=> $pagination->first()]);
    }

    /**
     * @param string $keyword|null
     * @return EventRepository
     */
    public function withSearch(string $keyword = null): EventRepository
    {
        if ($keyword) {
            $this->query['keyword'] = $keyword;
        }
        return $this;
    }

    public function withPagination(int $page = 0, int $size = 20)
    {
        if ($page && $size) {
            $this->query['page'] = $page;
            $this->query['size'] = $size;
        }
        return $this;
    }


}
