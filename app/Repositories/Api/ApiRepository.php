<?php
namespace App\Repositories\Api;

use App\Repositories\Repository;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Collection;
use mysql_xdevapi\Exception;

/**
 * Class ApiRepository
 * @package App\Repositories\Api
 */
abstract class ApiRepository implements Repository
{
    protected $apiClient;
    protected $query = [];

    public function __construct(ClientInterface $apiClient)
    {
        $this->apiClient = $apiClient;
    }

    protected function getBaseUri(): string
    {
        return $this->apiClient->getConfig('base_uri');
    }

    protected function getEndpoint(): string
    {
        return $this->apiClient->getConfig('endpoint');
    }

    protected function getApiKey(): string
    {
        return $this->apiClient->getConfig('apikey');
    }

    /**
     * @return Collection
     * @throws ClientException
     */
    public function get(): Collection
    {
        try {
            $response = $this->apiClient->request('get', $this->getBaseUri() . $this->getEndpoint(),[
                'query' => array_merge(['apikey' => $this->getApiKey()], $this->query)
            ]);
            return collect(json_decode($response->getBody()->getContents(), true));
        } catch (ClientException $clientException) {
           switch($clientException->getCode()) {
               case 401:
                   throw new \Exception('Access to the API returned a 401 error - Unauthorised', 401);
                   break;
               case 404:
                   throw new \Exception('Access to the API returned a 404 error - Not found', 404);
                   break;

               default:
                   throw $clientException;

           }
        }


    }

}
