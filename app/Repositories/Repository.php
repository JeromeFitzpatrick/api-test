<?php


namespace App\Repositories;


use Illuminate\Support\Collection;

interface Repository
{
    public function get(): Collection;
}
