<?php


namespace App\Repositories\Common;


/**
 * Interface Pagination
 * @package App\Repositories\Api
 */
interface Pagination
{
    /**
     * @return int
     */
    public function getCurrentPage(): int;

    /**
     * @return int|null
     */
    public function getNextPage(): ?int;

    /**
     * @return int|null
     */
    public function getPreviousPage(): ?int;

    /**
     * @return int
     */
    public function getPageSize(): int;

    /**
     * @return int
     */
    public function getLastPage(): int;

    /**
     * @return bool
     */
    public function hasNextPage(): bool;

    /**
     * @return bool
     */
    public function hasPreviousPage(): bool;

}
