<?php


namespace App\Repositories\Common;


use App\Models\Common\BasicPopo;
use App\Repositories\Common\Pagination;

/**
 * Class PaginationPopo
 * @package App\Repositories\Api
 */
class PaginationPopo extends BasicPopo implements Pagination
{

    /**
     * @var
     */
    protected $size;
    /**
     * @var
     */
    protected $totalElements;
    /**
     * @var
     */
    protected $totalPages;

    /**
     * @var
     */
    protected $number;

    /**
     * @var array
     */
    protected $attributes = [];
    /**
     * @var array
     */
    protected $config = [
        'size' => ['type'=>'integer',],
        'totalElements' => ['type'=>'integer',],
        'totalPages' => ['type'=>'integer',],
        'number' => ['type'=>'integer',],
    ];

    /**
     * PaginationPopo constructor.
     * @param array $attributes
     */
    public function __construct(array $attributes)
    {
        $this->attributes = $attributes;
        $this->hydrate();
    }


    /**
     * @return int
     */
    public function getCurrentPage(): int
    {
        return $this->number;
    }

    /**
     * @return int|null
     */
    public function getNextPage(): ?int
    {
        return $this->number < $this->totalPages ? $this->number + 1 : null;
    }

    /**
     * @return int|null
     */
    public function getPreviousPage(): ?int
    {
        return $this->number > 0 ? $this->number - 1 : null;
    }

    /**
     * @return int
     */
    public function getPageSize(): int
    {
        return $this->size;
    }

    /**
     * @return int
     */
    public function getLastPage(): int
    {
        return $this->totalPages > 0 ? $this->totalPages - 1 : 0;
    }

    public function hasNextPage(): bool
    {
        return !is_null($this->getNextPage());
    }

    public function hasPreviousPage(): bool
    {
        return !is_null($this->getPreviousPage());
    }
}
