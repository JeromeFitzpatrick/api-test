<?php


namespace App\Repositories;


interface EventRepository extends Repository
{
    public function withSearch(string $keyword = null);
    public function withPagination(int $page = 0, int $size=20);
}
