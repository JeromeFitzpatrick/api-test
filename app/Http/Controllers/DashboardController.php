<?php

namespace App\Http\Controllers;

use App\Exceptions\DataExceptionHandlerInterface;
use App\Repositories\EventRepository;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    protected $eventRepository;
    protected $dataExceptionHandler;

    public function __construct(EventRepository $eventRepository, DataExceptionHandlerInterface $dataExceptionHandler)
    {
        $this->eventRepository = $eventRepository;
        $this->dataExceptionHandler = $dataExceptionHandler;
        $this->middleware('auth');

    }

    /**
     * Display the dashboard
     *
     * @return Renderable
     * @throws \Exception
     */
    public function index(Request $request)
    {
        try {
            $events = $this->eventRepository
                ->withSearch($request->get('keyword'))
                ->withPagination($request->get('page', 0), $request->get('size', 20))
                ->get();
            return view('dashboard', [
                'events'=>$events['data'],
                'pagination'=>$events['pagination'],
                'query' => $request->get('keyword') ? 'keyword=' . $request->get('keyword') : '',
            ]);
        } catch (\Exception $exception) {
            if ($error = $this->dataExceptionHandler->handle($exception)) {
                return view('dashboard', $error);
            }
            throw $exception;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

}
