<?php

namespace App\Providers;

use App\Exceptions\DataExceptionHandler;
use App\Exceptions\DataExceptionHandlerInterface;
use App\Models\Event\PriceRanges;
use App\Models\Event\PriceRangesPopo;
use App\Repositories\Api\ApiEventRepository;
use App\Repositories\EventRepository;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(DataExceptionHandlerInterface::class, DataExceptionHandler::class);
        $this->app->bind(EventRepository::class, ApiEventRepository::class);

        $this->app->when([ApiEventRepository::class])
            ->needs(ClientInterface::class)
            ->give(function ($app) {
                return new Client(
                    [
                        'base_uri' => config('api.base_uri'),
                        'endpoint' => config('api.events.endpoint'),
                        'apikey' => env('EVENTS_API_KEY'),
                    ]
                );
            });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
