<?php


namespace App\Exceptions;


interface DataExceptionHandlerInterface
{
    public function handle(\Exception $exception);
}
