<?php


namespace App\Exceptions;


class DataExceptionHandler implements DataExceptionHandlerInterface
{

    public function handle(\Exception $exception)
    {
        switch($exception->getCode()) {
            case 401:
                return ['error'=>'Unauthorized - please ensure you provide the correct credentials to access this data.'];
                break;
            case 403:
                return ['error'=>'Unauthorized - you do note have permission to view this resource.'];
                break;
            case 404:
                return ['error'=>'Not Found - unable to locate this resource.'];
                break;
            case 422:
                return ['error'=>'Unprocessable Entity - please ensure your parameter and data sent with the request are correct.'];
                break;
            case 400:
                return ['error'=>'Bad Request - the request was received but is invalid.'];
                break;
            case 405:
                return ['error'=>'Method not allowed - the request method is not allowed.'];
                break;
                case 500:
                return ['error'=>'Server Error - please contact the software administrator.'];
            default:
                return null;

        }

    }
}
